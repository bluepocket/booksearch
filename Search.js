'use strict';

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
} from 'react-native';

var SearchBooks = require('./SearchBooks');

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

class Search extends React.Component {
    render() {
        return (
            <NavigatorIOS
                style={styles.container}
                initialRoute={{
            title: 'Search Books',
            component: SearchBooks
        }}/>
        );
    }
}

module.exports = Search;
