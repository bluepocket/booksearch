'use strict';

import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
} from 'react-native';

var BookList = require('./BookList');

var styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

class Featured extends React.Component {
    render() {
        return (
            <NavigatorIOS
                style={styles.container}
                initialRoute={{
            title: 'Featured Books',
            component: BookList
            }}/>
        );
    }
}

module.exports = Featured;
